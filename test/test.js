#!/usr/bin/env node

'use strict';

require('chromedriver');

var execSync = require('child_process').execSync,
    expect = require('expect.js'),
    fs = require('fs'),
    path = require('path'),
    superagent = require('superagent'),
    webdriver = require('selenium-webdriver');

var by = require('selenium-webdriver').By,
    until = require('selenium-webdriver').until,
    Builder = require('selenium-webdriver').Builder;

describe('Application life cycle test', function () {
    this.timeout(0);

    var server, browser = new Builder().forBrowser('chrome').build();
    var username = process.env.USERNAME;
    var password = process.env.PASSWORD;

    before(function (done) {
        if (!process.env.USERNAME) return done(new Error('USERNAME env var not set'));
        if (!process.env.PASSWORD) return done(new Error('PASSWORD env var not set'));

        var seleniumJar= require('selenium-server-standalone-jar');
        var SeleniumServer = require('selenium-webdriver/remote').SeleniumServer;
        server = new SeleniumServer(seleniumJar.path, { port: 4444 });
        server.start();

        done();
    });

    after(function (done) {
        browser.quit();
        server.stop();
        done();
    });

    var LOCATION = 'test';
    var TEST_TIMEOUT = 10000;
    var app;

    function visible(selector) {
        return browser.wait(until.elementLocated(selector), TEST_TIMEOUT).then(function () {
            return browser.wait(until.elementIsVisible(browser.findElement(selector)), TEST_TIMEOUT);
        });
    }

    function login(username, password, callback) {
        browser.get('https://' + app.fqdn + '/scp/login.php').then(function () {
            return visible(by.id('name'));
        }).then(function () {
            return browser.findElement(by.id('name')).sendKeys(username);
        }).then(function () {
            return browser.findElement(by.id('pass')).sendKeys(password);
        }).then(function () {
            return browser.findElement(by.tagName('form')).submit();
        }).then(function () {
            return browser.wait(until.elementLocated(by.xpath('//a[text()="Dashboard"]')), TEST_TIMEOUT);
        }).then(function () {
            callback();
        });
    }

    function logout(callback) {
        browser.get('https://' + app.fqdn + '/scp/index.php').then(function () {
            return browser.findElement(by.xpath('//a[text()="Log Out"]')).click();
        }).then(function () {
            return browser.sleep(2000);
        }).then(function () {
            callback();
        });
    }

    function checkHelpdeskUrl(callback) {
        browser.get('https://' + app.fqdn + '/scp/settings.php').then(function () {
            return browser.wait(until.elementLocated(by.xpath(`//input[@value="https://${app.fqdn}"]`)), TEST_TIMEOUT);
        }).then(function () {
            callback();
        });
    }

    function checkIsDashboard(callback) {
        browser.get('https://' + app.fqdn + '/scp/index.php').then(function () {
            return browser.wait(until.elementLocated(by.xpath('//a[contains(text(),"Dashboard")]')), TEST_TIMEOUT);
        }).then(function () {
            callback();
        });
    }

    function getAppInfo() {
        var inspect = JSON.parse(execSync('cloudron inspect'));
        app = inspect.apps.filter(function (a) { return a.location === LOCATION || a.location === LOCATION + '2'; })[0];
        expect(app).to.be.an('object');
    }

    function addLdapUser() {
        fs.writeFileSync('/tmp/csv', `Firstname,Lastname,${process.env.EMAIL},${process.env.USERNAME}`);
        execSync(`cloudron push --app ${app.fqdn} /tmp/csv /tmp/csv`);
        execSync(`cloudron exec --app ${app.fqdn} -- bash -c 'php manage.php agent --backend=ldap import < /tmp/csv'`, { encoding: 'utf8', stdio: 'inherit' });
        // after importing user, role is not set
        execSync(`cloudron exec --app ${app.fqdn} -- bash -c 'mysql --user=\${CLOUDRON_MYSQL_USERNAME} --password=\${CLOUDRON_MYSQL_PASSWORD} --host=\${CLOUDRON_MYSQL_HOST} \${CLOUDRON_MYSQL_DATABASE} -e "UPDATE ost_staff SET role_id=1;"'`, { encoding: 'utf8', stdio: 'inherit' });
    }

    xit('build app', function () {
        execSync('cloudron build', { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('install app', function () {
        execSync('cloudron install --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        getAppInfo();
    });
    it('can admin login', login.bind(null, 'root', 'changeme'));
    it('check helpdesk url', checkHelpdeskUrl);
    it('is dashboard', checkIsDashboard);
    it('can logout', logout);

    it('can add ldap user', addLdapUser);
    it('can ldap login', login.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('is dashboard', checkIsDashboard);
    it('can logout', logout);

    it('backup app', function () {
        execSync('cloudron backup create --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can admin login', login.bind(null, 'root', 'changeme'));
    it('check helpdesk url', checkHelpdeskUrl);
    it('is dashboard', checkIsDashboard);
    it('can logout', logout);

    it('restore app', function () {
        execSync('cloudron restore --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    it('can admin login', login.bind(null, 'root', 'changeme'));
    it('check helpdesk url', checkHelpdeskUrl);
    it('is dashboard', checkIsDashboard);
    it('can logout', logout);

    it('move to different location', function () {
        execSync('cloudron configure --location ' + LOCATION + '2 --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        getAppInfo();
    });

    it('can admin login', login.bind(null, 'root', 'changeme'));
    it('check helpdesk url', checkHelpdeskUrl);
    it('is dashboard', checkIsDashboard);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // non sso
    it('install app (no sso)', function () {
        execSync('cloudron install --no-sso --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        getAppInfo();
    });
    it('can admin login', login.bind(null, 'root', 'changeme'));
    it('check helpdesk url', checkHelpdeskUrl);
    it('is dashboard', checkIsDashboard);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });

    // test update
    it('can install app', function () {
        execSync('cloudron install --appstore-id com.osticket.cloudronapp --location ' + LOCATION, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        getAppInfo();
    });

    it('can add ldap user', addLdapUser);

    it('can update', function () {
        execSync('cloudron update --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
        getAppInfo();
    });

    it('can admin login', login.bind(null, 'root', 'changeme'));
    it('check helpdesk url', checkHelpdeskUrl);
    it('is dashboard', checkIsDashboard);
    it('can logout', logout);

    it('can ldap login', login.bind(null, process.env.USERNAME, process.env.PASSWORD));
    it('is dashboard', checkIsDashboard);
    it('can logout', logout);

    it('uninstall app', function () {
        execSync('cloudron uninstall --app ' + app.id, { cwd: path.resolve(__dirname, '..'), stdio: 'inherit' });
    });
});
