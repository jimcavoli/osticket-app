The admin interface is located at `/scp/login.php`. This app is pre-setup with an admin account.
The initial credentials are:

**Username**: root <br/>
**Password**: changeme <br/>
**Email**: admin@server.local <br/>

Please change the admin password and email immediately.

See the [admin checklist](https://cloudron.io/documentation/apps/osticket#admin-checklist) before
starting to use the app.

