#!/bin/bash

set -eu

mkdir -p /run/osticket/session /app/data/attachments

mysql="mysql --user=${CLOUDRON_MYSQL_USERNAME} --password=${CLOUDRON_MYSQL_PASSWORD} --host=${CLOUDRON_MYSQL_HOST} ${CLOUDRON_MYSQL_DATABASE} "

function update_settings() {
    echo "==> Updating settings"

    $mysql -e "UPDATE ost_config SET value='${CLOUDRON_APP_ORIGIN}' WHERE \`key\`='helpdesk_url'"

    # fixup the managed email
    smtp_userpass=$(php /app/pkg/encrypt.php "${CLOUDRON_MAIL_SMTP_PASSWORD}" "${CLOUDRON_MAIL_SMTP_USERNAME}")
    $mysql -e "UPDATE ost_email SET smtp_active=1, smtp_host='${CLOUDRON_MAIL_SMTP_SERVER}', smtp_port=${CLOUDRON_MAIL_SMTP_PORT}, smtp_auth=1, smtp_auth_creds=1, smtp_userid='${CLOUDRON_MAIL_SMTP_USERNAME}', smtp_userpass='${smtp_userpass}' WHERE email='${CLOUDRON_MAIL_FROM}'"

    # fixup ldap (https://github.com/osTicket/osTicket-plugins/blob/develop/auth-ldap/config.php#L216)
    if [[ -n "${CLOUDRON_LDAP_SERVER:-}" ]]; then
        ldap_bind_pwd=$(php /app/pkg/encrypt.php "${CLOUDRON_LDAP_BIND_PASSWORD}" "core")
        $mysql -e "UPDATE ost_config SET value='${CLOUDRON_APP_DOMAIN}' WHERE namespace='plugin.2' AND \`key\`='domain';" \
            -e "UPDATE ost_config SET value='${CLOUDRON_LDAP_URL}' WHERE namespace='plugin.2' AND \`key\`='servers';" \
            -e "UPDATE ost_config SET value='${CLOUDRON_LDAP_USERS_BASE_DN}' WHERE namespace='plugin.2' AND \`key\`='search_base';" \
            -e "UPDATE ost_config SET value='${CLOUDRON_LDAP_BIND_DN}' WHERE namespace='plugin.2' AND \`key\`='bind_dn';" \
            -e "UPDATE ost_config SET value='${ldap_bind_pwd}' WHERE namespace='plugin.2' AND \`key\`='bind_pw';"
    fi
}

function setup() {
    readonly name="Acme Inc"
    # the setup script creates noreply@ and alerts@. The alerts@ is used for alert/reset/invites etc
    # this is created with email_id as 1 which is referenced below
    readonly support_email="support@${CLOUDRON_MAIL_DOMAIN}"

    readonly username=root # admin does not work
    readonly password=changeme
    readonly email="admin%40server.local"

    while [[ ! -f "/var/run/apache2/apache2.pid" ]]; do
        echo "==> Waiting for apache2 to start"
        sleep 1
    done
 
    rm -f /run/osticket/setup
    ln -s /app/code/upload/setup.orig /run/osticket/setup 

    echo "==> Running first time setup"
    if ! curl --fail -v 'http://localhost:8000/setup/install.php' --data "s=install&name=${name}&email=${support_email}&lang_id=en_US&fname=OSTicket&lname=Admin&admin_email=${email}&username=${username}&passwd=${password}&passwd2=${password}&prefix=ost_&dbhost=${CLOUDRON_MYSQL_HOST}&dbname=${CLOUDRON_MYSQL_DATABASE}&dbuser=${CLOUDRON_MYSQL_USERNAME}&dbpass=${CLOUDRON_MYSQL_PASSWORD}&timezone=America%2FLos_Angeles"; then
        echo "==> Failed to setup"
        return 1
    fi

    rm -f /run/osticket/setup
    echo "==> Setup complete"

    # remove auto-created noreply mailbox
    $mysql -e "DELETE FROM ost_email WHERE email='noreply@${CLOUDRON_MAIL_DOMAIN}'"

    # delete auto-created support mailbox. set everything to alert mailbox. user can setup support mailbox by hand
    $mysql -e "UPDATE ost_config SET value=2 WHERE \`key\`='default_smtp_id';" \
           -e "UPDATE ost_config SET value=2 WHERE \`key\`='alert_email_id';" \
           -e "UPDATE ost_config SET value=2 WHERE \`key\`='default_email_id';" \
           -e "DELETE FROM ost_email WHERE email='${support_email}'"

    # rename the alerts box to cloudron sendmail addon
    $mysql -e "UPDATE ost_email SET email='${CLOUDRON_MAIL_FROM}' WHERE email='alerts@${CLOUDRON_MAIL_DOMAIN}'"

    # enable and configure attachment plugin
    $mysql -e "INSERT INTO ost_plugin(name, install_path, isphar, isactive, installed) VALUES('Attachments on the filesystem', 'plugins/storage-fs.phar', 1, 1, '2020-04-04 05:15:05');" \
           -e "INSERT INTO ost_config(namespace, \`key\`, value) VALUES('plugin.1', 'uploadpath', '/app/data/attachments')"

    # rename the alerts box to cloudron sendmail addon
    $mysql -e "UPDATE ost_config SET value=1 WHERE \`key\`='enable_mail_polling'"

    # enable and configure ldap
    if [[ -n "${CLOUDRON_LDAP_SERVER:-}" ]]; then
        $mysql -e "INSERT INTO ost_plugin(name, install_path, isphar, isactive, installed) VALUES('LDAP Authentication and Lookup', 'plugins/auth-ldap.phar', 1, 1, '2020-04-04 05:15:05')"

        $mysql -e "INSERT INTO ost_config(namespace, \`key\`, value) VALUES('plugin.2', 'domain', '${CLOUDRON_APP_DOMAIN}');" \
            -e "INSERT INTO ost_config(namespace, \`key\`, value) VALUES('plugin.2', 'dns', '');" \
            -e "INSERT INTO ost_config(namespace, \`key\`, value) VALUES('plugin.2', 'servers', '${CLOUDRON_LDAP_URL}');" \
            -e "INSERT INTO ost_config(namespace, \`key\`, value) VALUES('plugin.2', 'tls', '0');" \
            -e "INSERT INTO ost_config(namespace, \`key\`, value) VALUES('plugin.2', 'bind_dn', '${CLOUDRON_LDAP_BIND_DN}');" \
            -e "INSERT INTO ost_config(namespace, \`key\`, value) VALUES('plugin.2', 'bind_pw', 'willbesetbyupdate');" \
            -e "INSERT INTO ost_config(namespace, \`key\`, value) VALUES('plugin.2', 'search_base', '${CLOUDRON_LDAP_USERS_BASE_DN}');" \
            -e "INSERT INTO ost_config(namespace, \`key\`, value) VALUES('plugin.2', 'schema', '{\"msad\":\"Microsoft\\u00ae Active Directory\"}');" \
            -e "INSERT INTO ost_config(namespace, \`key\`, value) VALUES('plugin.2', 'auth-staff', '1');" \
            -e "INSERT INTO ost_config(namespace, \`key\`, value) VALUES('plugin.2', 'auth-client', '0');"
    fi

    update_settings
}

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

echo "==> Changing permissions"
chown www-data.www-data /app/data /run/osticket

if [[ ! -f /app/data/ost-config.php ]]; then
    echo "==> Detected first run"

    cp /app/code/upload/include/ost-sampleconfig.php /app/data/ost-config.php
    chown www-data.www-data /app/data/ost-config.php

    APACHE_CONFDIR="" source /etc/apache2/envvars
    rm -f "${APACHE_PID_FILE}"
    setup &
else
    update_settings
fi

echo "==> Starting osticket"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND

